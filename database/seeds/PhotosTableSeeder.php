<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('photos')->insert([
            [
                'item_id' => 1,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 1,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 1,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 1,
                'file_name' => '4.png',
                'priority' => 0,
            ],
            [
                'item_id' => 1,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 2,
                'file_name' => '1.png',
                'priority' => 1,
            ],
            [
                'item_id' => 2,
                'file_name' => '2.png',
                'priority' => 0,
            ],
            [
                'item_id' => 2,
                'file_name' => '3.png',
                'priority' => 0,
            ],
            [
                'item_id' => 2,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 2,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 2,
                'file_name' => '6.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 2,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 3,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 3,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 3,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 3,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 3,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 4,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 4,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 4,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 4,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 4,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 5,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 5,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 5,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 5,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 5,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 6,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 6,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 6,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 6,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 6,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 7,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 7,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 7,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 7,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 7,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 8,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 8,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 8,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 8,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 8,
                'file_name' => '5.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 9,
                'file_name' => '1.jpg',
                'priority' => 1,
            ],
            [
                'item_id' => 9,
                'file_name' => '2.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 9,
                'file_name' => '3.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 9,
                'file_name' => '4.jpg',
                'priority' => 0,
            ],
            [
                'item_id' => 9,
                'file_name' => '5.jpg',
                'priority' => 0,
            ]
        ]);
    }
}
