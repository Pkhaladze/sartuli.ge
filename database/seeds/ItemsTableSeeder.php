<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('items')->insert([
            [
                'name' => "აირგამის ბლოკი",
                'units' => "მ/კუბი",
                'price' => 250,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "აირფოროვანი ავტოკლავში გამტკიცებული ბლოკი ზომა - 
                						600X250X100, 600X250X150, 600X250X200, 600X250X250, 
                						600X250X300, 600X250X400, წონა 500 კგ მ/კუბზე. გამოიყენება 
                						მაღალი სიზუსტის წყობისთვის. მისი ფორმები უზრუნველყოფის 
                						წყობის სიმარტივეს. არ წარმოქმნის უსწორმასწორო ზედაპირს რაც
                						 თავიდან აცილებს გაჯით ლესვას. ინტერიერში ზედაპირი იფარება
                						  შპაკლით. ექსტერიერში იშპაკლება ბადის არმირებით. გააჩნია მარალი თბოსაიზოლაციო და ხმის დახშობის ფუნქცია.",
                'description' => "აირფოროვანი ავტოკლავში გამტკიცებული ბლოკი ზომა - 
                                        600X250X100, 600X250X150, 600X250X200, 600X250X250, 
                                        600X250X300, 600X250X400, წონა 500 კგ მ/კუბზე. გამოიყენება 
                                        მაღალი სიზუსტის წყობისთვის. მისი ფორმები უზრუნველყოფის 
                                        წყობის სიმარტივეს. არ წარმოქმნის უსწორმასწორო ზედაპირს რაც
                                         თავიდან აცილებს გაჯით ლესვას. ინტერიერში ზედაპირი იფარება
                                          შპაკლით. ექსტერიერში იშპაკლება ბადის არმირებით. გააჩნია მარალი თბოსაიზოლაციო და ხმის დახშობის ფუნქცია."
            ],
            [
                'name' => "თაბაშირის ბლოკი",
                'units' => "მ/კვ",
                'price' => 25,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "ტიხრის თაბაშირ ბლოკი ზომა - 80X667, წონა 20-22 კგ 
                                        გამოიყენება მაღალი სიზუსტის წყობისთვის. მისი ფორმები უზრუნველყოფის წყობის სიმარტივეს. არ წარმოქმნის უსწორმასწორო ზედაპირს რაც თავიდან აცილებს გაჯით ლესვას. ზედაპირი იფარება შპაკლით.",
                'description' => "ტიხრის თაბაშირ ბლოკი ზომა - 80X667, წონა 20-22 კგ 
                                        გამოიყენება მაღალი სიზუსტის წყობისთვის. მისი ფორმები უზრუნველყოფის წყობის სიმარტივეს. არ წარმოქმნის უსწორმასწორო ზედაპირს რაც თავიდან აცილებს გაჯით ლესვას. ზედაპირი იფარება შპაკლით."
            ],
            [
                'name' => "საძირკველი",
                'units' => "მ/კვ",
                'price' => 111,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული 
                                        დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და
                                        საძირკვლის მოწყობა",
                'description' => "არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული 
                                        დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და
                                        საძირკვლის მოწყობა"
            ],
            [
                'name' => "შავი კარკასი​",
                'units' => "მ/კვ",
                'price' => 444,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, 
                            გადახურვა​ + (არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა)",
                'description' => "რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, 
                            გადახურვა​ + (არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა)"
            ],
            [
                'name' => "თეთრი კარკასი​",
                'units' => "მ/კვ",
                'price' => 555,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, გალესილი შიდა კედლები, გალესილი ფასადი, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი, ელექტრო წერტილები, ვენტილაცია და სველი წერტილები, სახურავის ჰიდროიზოლაცია​ +(არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, გადახურვა)​",
                'description' => "რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, გალესილი შიდა კედლები, გალესილი ფასადი, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი, ელექტრო წერტილები, ვენტილაცია და სველი წერტილები, სახურავის ჰიდროიზოლაცია​ +(არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, გადახურვა)​"
            ],
            [
                'name' => "მინი რემონტი",
                'units' => "მ/კვ",
                'price' => 777,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "დაბალბიუჯეტური რემონტი მოკლევადიანი პერიოდისთვის​
                    +(არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, გადახურვა, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, გალესილი შიდა კედლები, გალესილი ფასადი, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი, ელექტრო წერტილები, ვენტილაცია და სველი წერტილები, სახურავის ჰიდროიზოლაცია)​",
                'description' => "დაბალბიუჯეტური რემონტი მოკლევადიანი პერიოდისთვის​
                    +(არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, გადახურვა, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, გალესილი შიდა კედლები, გალესილი ფასადი, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი, ელექტრო წერტილები, ვენტილაცია და სველი წერტილები, სახურავის ჰიდროიზოლაცია)​"
            ],
            [
                'name' => "სრული რემონტი",
                'units' => "მ/კვ",
                'price' => 888,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "​სრულყოფილი რემონტი რომელიც მოიცავს საშუალო ფასიან (30 
                    ₾არიანი შპალერი, ლამინატი, კაფელი და ა.შ.) მასალებს​ +(არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, გადახურვა, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, გალესილი შიდა კედლები, გალესილი ფასადი, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი, ელექტრო წერტილები, ვენტილაცია და სველი წერტილები, სახურავის ჰიდროიზოლაცია)​​",
                'description' => "სრულყოფილი რემონტი რომელიც მოიცავს საშუალო ფასიან (30 
                    ₾არიანი შპალერი, ლამინატი, კაფელი და ა.შ.) მასალებს​ +(არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება, გადახურვა, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, გალესილი შიდა კედლები, გალესილი ფასადი, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი, ელექტრო წერტილები, ვენტილაცია და სველი წერტილები, სახურავის ჰიდროიზოლაცია)"
            ],
            [
                'name' => "კორპორატიული თეთრი კარკასი​ ",
                'units' => "მ/კვადრატი შიდა ფართი​",
                'price' => 888,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "​
                - გალესილი ოთახები, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი.​
                - ლიფტი, მოაჯირები, სახურავის ჰიდროიზოლაცია, მოპირკეთებული ფასადი და სადარბაზო.​
                - შენობის და ოთახების დაქსელვა ელექტრო სისტემით, ბუნებრივი აირით ვენტილაციით და წყლით.​
                - არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება.​",
                'description' => "
                - გალესილი ოთახები, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი.​
                - ლიფტი, მოაჯირები, სახურავის ჰიდროიზოლაცია, მოპირკეთებული ფასადი და სადარბაზო.​
                - შენობის და ოთახების დაქსელვა ელექტრო სისტემით, ბუნებრივი აირით ვენტილაციით და წყლით.​
                - არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება.​"
            ],
            [
                'name' => "კორპორატიული რემონტი სრული+​",
                'units' => "მ/კვადრატი შიდა ფართი​",
                'price' => 1200,
                'sale' => null,
                'quantity' => 10,
                'status' => "active",
                'short_description' => "​
                - ცენტრალური გათბობის ქვაბი, საშუალო ფასიანი (30 ₾არიანი შპალერი, ლამინატი, კაფელი და ა.შ.) სარემონტო მასალები დამკვეთის არჩევანით.​
                - გალესილი ოთახები, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი.​
                - ლიფტი, მოაჯირები, სახურავის ჰიდროიზოლაცია, მოპირკეთებული ფასადი და სადარბაზო.​
                - შენობის და ოთახების დაქსელვა ელექტრო სისტემით, ბუნებრივი აირით ვენტილაციით და წყლით.​
                - არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება.​",
                'description' => "
                - ცენტრალური გათბობის ქვაბი, საშუალო ფასიანი (30 ₾არიანი შპალერი, ლამინატი, კაფელი და ა.შ.) სარემონტო მასალები დამკვეთის არჩევანით.​
                - გალესილი ოთახები, რკინის კარი, მეტალო-პლასტმასის კარ-ფანჯრები, ცენტრალური გათბობის მილები, მოჭიმული იატაკი, გიფსოკარდონის ჭერი.​
                - ლიფტი, მოაჯირები, სახურავის ჰიდროიზოლაცია, მოპირკეთებული ფასადი და სადარბაზო.​
                - შენობის და ოთახების დაქსელვა ელექტრო სისტემით, ბუნებრივი აირით ვენტილაციით და წყლით.​
                - არქიტექტურა და ტოპოგრაფია, გეოლოგიური და კონსტრუქციული დასკვნები, ექსპერტიზა და ხარჯთაღრიცხვა, ნებართვა და საძირკვლის მოწყობა, რკინა-ბეტონის მონოლითური კონსტრუქცია, კედლების ამოშენება.​"
            ]
        ]);
    }
}
