<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            [
                'black_home' => 360,
                'white_home' => 455,
                'black_comercial' => 760,
                'white_comercial' => 860
            ]
        ]);
    }
}