<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name' => "გამყოფი კედლის მასალები",
                'parent' => 0,
            ],
            [
                'name' => "მასალები",
                'parent' => 0,
            ],
            [
                'name' => "კედლის მასალები",
                'parent' => 0,
            ],
            [
                'name' => "მშენებლობა",
                'parent' => 0,
            ],
            [
                'name' => "კერძო სახლები",
                'parent' => 0,
            ],
            [
                'name' => "მრავალბინიანი",
                'parent' => 0,
            ]
        ]);
    }
}
