<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Route::get('/', 'Pages\SiteController@index');
Route::get('/about', 'Pages\SiteController@about');
Route::get('/shop_sidebar', 'Pages\SiteController@shop_sidebar');
Route::get('/single_shop', 'Pages\SiteController@single_shop');
Route::get('/shoping_cart', 'Pages\SiteController@shoping_cart');
Route::get('/checkout', 'Pages\SiteController@checkout');
Route::get('/blog', 'Pages\SiteController@blog');
Route::get('/single_blog', 'Pages\SiteController@single_blog');
Route::get('/gallery', 'Pages\SiteController@gallery');
Route::get('/single_gallery', 'Pages\SiteController@single_gallery');
//Route::get('/services', 'Pages\SiteController@services');
Route::get('/contact', 'Pages\SiteController@contact');
Route::resource('/items', 'Pages\ItemController');
Auth::routes();


Route::resource('/admin', 'AdminController');

//getiing image upload and show routes
Route::get('img/items/{id}/{type}/{filename}', function ($id, $type, $filename)
{
    $path = storage_path() . '/app/img/items/'. $id . '/' . $type . '/' . $filename;


    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});