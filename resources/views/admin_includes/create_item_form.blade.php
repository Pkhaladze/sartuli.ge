<form method="POST" action="admin" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="name" class="col-form-label">სახელი</label>
			<input type="text" class="form-control" id="name" name="name" placeholder="სახელი">
		</div>
		<div class="form-group col-md-4">
			<label for="units" class="col-form-label">ზომის ერთეული</label>
			<input type="text" class="form-control" id="units" name="units" placeholder="ზომის ერთეული">
		</div>
		<div class="form-group col-md-4">
			<label for="price" class="col-form-label">ფასი</label>
			<input type="text" class="form-control" id="price" name="price" placeholder="ფასი">
		</div>
		<div class="form-group col-md-4">
			<label for="sale" class="col-form-label">ფასდაკლებით</label>
			<input type="text" class="form-control" id="sale" name="sale" placeholder="ფასდაკლებით">
		</div>
		<div class="form-group col-md-4">
			<label for="quantity" class="col-form-label">რაოდენობა</label>
			<input type="text" class="form-control" id="quantity" name="quantity" placeholder="რაოდენობა">
		</div>
		<div class="form-group col-md-4">
			<label for="status" class="col-form-label">სტატუსი</label>
			<select  id="status" name="status" class="form-control">
				<option>active</option>
				<option>passive</option>
			</select>
		</div>

		<div class="form-group col-md-4">
			<label for="categories" class="col-form-label">კატეგორიები</label>  
		    <select id="categories" class="form-control input-md" name="categories">
		     <option></option>
		     @foreach ($categories as $category)
		         <option value="{{$category->id }}">{{ $category->name }}</option>
		     @endforeach
		   </select>
		</div>

	    <div class="clearfix"></div>
		<div class="form-group col-md-6">
			<label for="short_description" class="col-form-label">მოკლე აღწერა</label>
			<textarea rows="3" class="form-control" id="short_description" name="short_description" placeholder="მოკლე აღწერა"></textarea> 
		</div>
		<div class="form-group col-md-6">
			<label for="description" class="col-form-label">აღწერა</label>
			<textarea rows="3" class="form-control" id="description" name="description" placeholder="აღწერა"></textarea> 
		</div>
		<div class="clearfix"></div>
	</div>
    <div id="photo">
        <div class="form-group col-xs-11">
            <label for="file_input" class="col-lg-2">Photo-1</label>
            <input id="file_input" type="file" name="photos[]">
        </div>
    </div>
        <p type="button" class="btn btn-primary col-xs-1" id="upl_photo"> + </p>
    <div class="clearfix"></div>
<hr>
	<button type="submit" class="btn btn-primary">შენახვა</button>
</form>





<script type="text/javascript">
        $i = 2;
        $('#upl_photo').click(function(){
            $p =  '<div class="form-group col-xs-11"><label for="file_input" class="col-lg-2">Photo-'+$i+'</label><input id="file_input" type="file" name="photos[]"></div>';
            $i +=1;
            $("#photo").append($p);
        });

</script>
