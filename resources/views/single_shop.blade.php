@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="{{ url('/') }}"> Home </a> / <a href="{{ url('/items') }}"> პროდუქტები </a>/ დეტალურად </h4>
                    <h3 class="geo_mtavr">აღწერა</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->



    <!-- Start : Single Products Section 
==================================================-->
    <div class="shop-product-area detail">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-xs-12">
                    <div class="shop-products">
                        <div class="single-item-detail clearfix">
                            <div class="product-thumbnail">
                                <div class="single-thumbnail-big">
                                @foreach ($item->photos as $photo)
                                    <a href="{{ url('img/items') }}/{{ $item->id }}/large/{{ $photo->file_name }}" data-lightbox="{{ url('images/shop/5.jpg') }}" data-title="High Neck Bodycon Dress">
                                        <img src="{{ url('img/items') }}/{{ $item->id }}/large/{{ $photo->file_name }}" alt="">
                                    </a>
                                @endforeach
                                </div>
                                <div class="single-thumbnail-small row">
                                    @php $i = 0; @endphp
                                    @foreach ($item->photos as $photo)
                                        @php $i++;  @endphp
                                        <div class="{{ $i }} single-thumb" data-slick-index="{{ $i }}">
                                            <img src="{{ url('img/items') }}/{{ $item->id }}/small/{{ $photo->file_name }}" alt="thumb1">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7 col-xs-12">
                    <div class="allproduct-info">
                        <div class="tittle_product">
                            <a href="">{{ $item->name }}</a>
                            <div class="next_prev">
                                <a href="" class="next"><span class="arrow_carrot-left"></span></a>
                                <a href="" class="prev"><span class="arrow_carrot-right"></span></a>
                            </div>
                        </div>
                        <div class="allproduct-price-area">
                        @php
                            if(isset($item->sale)){
                                $price = $item->sale;
                                $dec = $item->price;
                                $type = '<i class="lari lari-normal"></i>';
                            }else{
                                $price = $item->price;
                                $dec = '';
                                $type = "";
                            }
                        @endphp
                            <span class="price">
                                <span class="n-amt">ფასი {{ $price }}   <i class="lari lari-normal"></i> {{ $item->units }}</span>
                            <span>
                                <del> @php echo $dec . " " . $type; @endphp </del>
                            </span>
                            </span>
                        </div>
                        <div class="p-content">
                        <hr>
                            {{-- <p class="content">Availability:<span> In stock </span></p> --}}
                            <p class="d-content">{{ $item->short_description }}</p>
                        </div>
                        <div class="categories-area">
                            <p class="category"><span>Categories :</span></p>
                            <ul>
                                @foreach ($item->categories as $category)
                                    <li><a >{{ $category->name }}, </a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cust-reviews-area detail">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-menu text-uppercase">
                        <ul>
                            <li role="presentation" class="active p-description">
                                <a href="#product-description" aria-controls="product-description" role="tab" data-toggle="tab">პროდუქტის აღწერა</a>
                            </li>
                        </ul>
                    </div>
                    <div class="reviews-content">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="product-description">
                                <p class="r-content">{{ $item->description }}</p>
                            </div>
                            <!-- End : description -->
                        </div>
                    </div>
                </div>
            </div> <!-- End : row -->
        </div><!-- End : container -->
    </div> <!-- End : Reviews  Section -->
    <!-- End: Single Products Section 
==================================================-->




@include('includes.footer')

@endsection