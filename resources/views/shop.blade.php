@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="{{ asset('/') }}"> Home </a> / პროდუქტები </h4>
                    <h3>პროდუქტების გვერდი</h3>
                </div>
            </div> <!-- End: .row -->
        </div> <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->

    <!-- Start: Header Section
==================================================-->
    <section class="shop_page">
        <div class="container">
            <div class="big-title">
                <div class="pull-left">
                    <h2>პროდუქტების კატალოგი</h2>
                </div>
            </div>
            <!-- end title -->
            <div class="row">
                <div id="content" class="col-md-12 col-sm-12 col-xs-12">
                    <div id="single-shop" class="row">
                        <div class="shop-wrapper text-center">

                            @foreach ($items as $item)                        
                            <div class="col-md-3 col-sm-6 col-xs-12">
                            <a href="{{ url('/items/' . $item->id) }}">
                                <div class="shop-item">
                                    <div class="shop-image">
                                        <div class="entry">
                                        @foreach ($item->photos as $photo)
                                            @if ($photo->priority == 1)
                                                <img src="{{ url('img/items')}}/{{ $item->id }}/large/{{ $photo->file_name }}" alt="" class="img-responsive">
                                            @endif
                                        @endforeach
                                        </div>
                                    </div>  <!-- End : shop image -->
                                    <div class="shop-title">
                                        <h2>{{ $item->name }}</h2>
                                        <span>{{isset($item->sale) ? $item->sale : $item->price }} <i class="lari lari-normal"></i></span>
                                    </div>  <!-- End : shop title  -->
                                </div>  <!-- End : shop item 1 -->
                            </a>
                            </div> <!-- End : col  -->
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- end content -->
            </div>
            <!-- end row -->
             <!-- End: pagination -->
        </div>
        <!-- End: container -->
    </section>
    <!-- End: Shop Section 
========================================-->
@include('includes.footer')
@endsection