<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <!-->
<html lang="zxx">
<!--
<![endif]-->

	<head>
	    <!-- TITLE OF SITE -->
	    <title>sartuli.ge</title>

	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="description" content="Dalan - Construction Building  HTML Template" />
	    <meta name="keywords" content="Dalan, parallax, one page, multipage, responsive, landing, html template" />
	    <meta name="author" content="Dalan">
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	    <!-- Favicons -->
	    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
	    <link rel="apple-touch-icon" href="{{ asset('images/apple-touch-icon.png') }}">
	    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-touch-icon-72x72.png') }}">
	    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-touch-icon-114x114.png') }}">


	    <!-- CSS Begins
		================================================== -->
	    <!--Animate Effect-->
	    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
	    <link href="{{ asset('css/hover.css') }}" rel="stylesheet">

	    <!--Pretty Photo for Image Preview-->
	    <link href="{{ asset('css/lightbox.css') }}" rel="stylesheet">
	    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet">

	    <!--Owl Carousel -->
	    <link href="{{ asset('css/owl.carousel.css') }}" rel="stylesheet">

	    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}" />
	    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css') }}" />


	    <!--BootStrap -->
	    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
	    <link href="{{ asset('css/normalize.css') }}" rel="stylesheet">

	    <!-- Main Style -->
	    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
	    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
		
		<!--DEFAULT COLOR/ CURRENTLY USING , Replace Your Color--> 
		<link rel="stylesheet" href="{{ asset('css/colors/default-color.css') }}">
	    
		<!--Replace Your Color Ends-->
		
	    <!-- Switcher Styles-->
	    <link rel="stylesheet" id="switcher-css" type="text/css" href="{{ asset('css/switcher.css') }}" media="all" />
	    <!-- END Switcher Styles -->

	    <!-- Template Color Demo Examples -->
	    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/colors/one.css') }}" title="alternate" media="all" /><!-- Color one -->   
	    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/colors/two.css') }}" title="next" media="all" /><!-- Color two--> 
	    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/colors/three.css') }}" title="bookmark" media="all" /><!-- Color three--> 
	    <link rel="alternate stylesheet" type="text/css" href="{{ asset('css/colors/four.css') }}" title="prefetch" media="all" /><!-- Color four-->   
	    <!-- END Template Color Demo Examples -->
 
	    <!--[if IE]>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

<body>

@yield('content')


    <!-- Scripts
========================================-->
    <!-- jquery -->
    <script src="{{ asset('js/jquery-1.12.4.min.js') }}" type="text/javascript"></script>
    <!-- Modernizer -->
    <script src="{{ asset('js/modernizr-2.6.2.min.js') }}" type="text/javascript"></script>
    <!-- plugins -->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- Use For Animation -->
    <script src="{{ asset('js/wow.min.js') }}"></script>
    <!-- Use For Menu -->
    <script src="{{ asset('js/menumaker.js') }}"></script>
    <!-- Sticky header -->
    <script src="{{ asset('js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('js/sticky-header.js') }}"></script>
    <!-- Use For carousel -->
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <!-- Use For Image Peview -->
    <script src="{{ asset('js/lightbox.min.js') }}"></script>
    <!-- Use For jquery Easing -->
    <script src="{{ asset('js/jquery.easing.js') }}"></script>
    <script src="{{ asset('js/jquery.easing.compatibility.js') }}"></script>
    <!-- Slick js -->
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <!-- Use For accrodion -->
    <script src="{{ asset('js/accrodion.min.js') }}"></script>
    <!-- Use For parallax -->
    <script src="{{ asset('js/jquery.stellar.min.js') }}"></script>
    <!-- Use For parallax -->
    <script src="{{ asset('js/parallax.min.js') }}"></script>
	<!-- Use For mixitup gallery -->
	<script src="{{ asset('js/jquery.mixitup.min.js') }}"></script>
	<script>
     // Pretty simple huh
		//var scene = document.getElementById('scene');
		//var parallax = new Parallax(scene);
	</script>
    <!--fswit Switcher   -->
    <script src="{{ asset('js/fswit.js') }}"></script>
    <!-- Custom Scripts
========================================-->
    <script src="{{ asset('js/main.js') }}"></script>
 
</body>

</html>