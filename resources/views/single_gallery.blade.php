@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="index.html"> Home </a> / Gallery </h4>
                    <h3>Our Gallery</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->


    <!--Start: Work Section 
==================================================-->
    <section class="single-work-page">
        <div class="container">
            <div class="row">
                <!-- portfolio item -->
                <div class="col-xs-12">
                    <div class="portfolioitem">
                        <div class="carousel slide" data-ride="carousel" id="blog-post-slider">
                            <a class="post-thumbnail" data-animsition-out="fade-out-up-sm" data-animsition-out-duration="500" href="single-blog.html">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active"><img alt="blog" src="images/shop/full_width.jpg">
                                    </div>
                                    <div class="item"><img alt="blog" src="images/slider/slider1.jpg">
                                    </div>
                                </div>
                                <!-- Controls -->
                            </a>
                            <a class="left carousel-control" data-slide="prev" href="#blog-post-slider" role="button">
                                <span aria-hidden="true" class="fa fa-angle-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" data-slide="next" href="#blog-post-slider" role="button">
                                <span aria-hidden="true" class="fa fa-angle-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="portfoliodesc">
                        <div class="row">
                            <div class="col-lg-8 col-sm-8">
                                <div class="project-details">
                                    <h4>PROJECT OVERVIEW</h4>
                                    <hr>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Invidunt ut labore et dolore magna stet clita kasd gubergrenLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren</p>
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren accusam et justo duo dolores et ea rebum. Stet clita kasd gubergrenLorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam</p>
                                </div>

                            </div>
                            <div class="col-lg-3 col-lg-offset-1 col-sm-4">
                                <div class="portfolio-single-detail">
                                    <h4>Project Detail</h4>
                                    <hr>
                                    <ul class="list-unstyled project-detail-list margin-bottom-50">
                                        <li><strong>Client</strong> David Jon</li>
                                        <li><strong>Company</strong> Computer Market</li>
                                        <li><strong>Date</strong> Jun 20, 2015</li>
                                        <li><strong>Work Type</strong> Repair, Laptop</li>
                                    </ul>
                                    <h4>Contact us now</h4>
                                    <hr>
                                    <ul class="list-inline social-icons">
                                        <li><a href="#" class="facebook-bg-hover"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li><a href="#" class="twitter-bg-hover"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li><a href="#" class="google-plus-bg-hover"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        <li><a href="#" class="pinterest-bg-hover"><i class="fa fa-pinterest"></i></a>
                                        </li>
                                        <li><a href="#" class="tumblr-bg-hover"><i class="fa fa-tumblr"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="related-portfolio">
                            <!-- Start: Heading -->
                            <div class="base-header">
                                <h3>Related Project</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
                            </div>
                            <!-- End: Heading -->
							<div class="row">
								<div class="owl-carousel owl-theme" id="team">
 
									<!-- Start: Related Work -->
									<div class="work-item">
										<div class="items"><img alt="" class="" src="images/shop/work-1.jpg"></div>
										<div class="mask2">
											<a href="gallery.html"><h4>BUILDINGS</h4></a>
											<a href="single-gallery.html"><p>Interior</p></a>
											<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
												<i class="icon_plus"></i>
											</a>
										</div>
									</div>
									<!-- End: Related Work-->
									<!-- Start: Related Work -->
									<div class="work-item">
										<div class="items"><img alt="" class="" src="images/shop/work-2.jpg"></div>
										<div class="mask2">
											<a href="gallery.html"><h4>BUILDINGS</h4></a>
											<a href="single-gallery.html"><p>Interior</p></a>
											<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
												<i class="icon_plus"></i>
											</a>
										</div>
									</div>
									<!-- End: Related Work-->
									<!-- Start: Related Work -->
									<div class="work-item">
										<div class="items"><img alt="" class="" src="images/shop/work-3.jpg"></div>
										<div class="mask2">
											<a href="gallery.html"><h4>BUILDINGS</h4></a>
											<a href="single-gallery.html"><p>Interior</p></a>
											<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
												<i class="icon_plus"></i>
											</a>
										</div>
									</div>
									<!-- End: Related Work-->
									<!-- Start: Related Work -->
									<div class="work-item">
										<div class="items"><img alt="" class="" src="images/shop/work-4.jpg"></div>
										<div class="mask2">
											<a href="gallery.html"><h4>BUILDINGS</h4></a>
											<a href="single-gallery.html"><p>Interior</p></a>
											<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
												<i class="icon_plus"></i>
											</a>
										</div>
									</div>
									<!-- End: Related Work-->
									<!-- Start: Related Work -->
									<div class="work-item">
										<div class="items"><img alt="" class="" src="images/shop/work-5.jpg"></div>
										<div class="mask2">
											<a href="gallery.html"><h4>BUILDINGS</h4></a>
											<a href="single-gallery.html"><p>Interior</p></a>
											<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
												<i class="icon_plus"></i>
											</a>
										</div>
									</div>
									<!-- End: Related Work-->
									<!-- Start: Related Work -->
									<div class="work-item">
										<div class="items"><img alt="" class="" src="images/shop/work-6.jpg"></div>
										<div class="mask2">
											<a href="gallery.html"><h4>BUILDINGS</h4></a>
											<a href="single-gallery.html"><p>Interior</p></a>
											<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
												<i class="icon_plus"></i>
											</a>
										</div>
									</div>
									<!-- End: Related Work-->
									<!-- Start: Related Work -->
									<div class="work-item">
										<div class="items"><img alt="" class="" src="images/shop/work-7.jpg"></div>
										<div class="mask2">
											<a href="gallery.html"><h4>BUILDINGS</h4></a>
											<a href="single-gallery.html"><p>Interior</p></a>
											<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
												<i class="icon_plus"></i>
											</a>
										</div>
									</div>
									<!-- End: Related Work-->
								</div>
							</div>
							<!---/.row -->
 
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ portfolio item -->
            </div>
            <!-- row /- -->
        </div>
        <!-- Container /- -->
    </section>
    <!-- End: Work Section 
==================================================-->

@include('includes.footer')
@endsection