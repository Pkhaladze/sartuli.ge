@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')


    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="index.html"> Home </a> / <a href="index.html"> Shop </a> / Checkout </h4>
                    <h3>Check out</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->


    <!-- Start: Checkout Section
==================================================-->
    <section class="checkout_section">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-12 col-sm-12 col-xs-12">
                    <form name="contactform" method="post" class="shopform">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="custom-title">
                                    <h3>BILLING DETAILS</h3>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>First Name *</label>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="First Name">
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Last Name *</label>
                                        <input type="text" name="name" id="name1" class="form-control" placeholder="Last Name">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Company Name</label>
                                        <input type="text" name="name" id="name2" class="form-control" placeholder="Company Name">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Address *</label>
                                        <input type="text" name="name" id="name3" class="form-control" placeholder="Address Line 1">
                                        <label>Address Line 2</label>
                                        <input type="text" name="name" id="name4" class="form-control" placeholder="Address Line 2">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Town / City *</label>
                                        <input type="text" name="name" id="name5" class="form-control" placeholder="Town / City">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>County *</label>
                                        <input type="text" name="name" id="name6" class="form-control" placeholder="County">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <label>Zip Code *</label>
                                        <input type="text" name="name" id="name7" class="form-control" placeholder="Zip Code">
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <label>Email *</label>
                                        <input type="text" name="name" id="name8" class="form-control" placeholder="Email Address">
                                        <label>Phone Number *</label>
                                        <input type="text" name="name" id="name9" class="form-control" placeholder="Phone Number">
                                    </div>

                                </div>
                                <!-- end row -->
                            </div>
                            <!-- end col -->
                            <div class="col-md-6">
                                <div class="custom-title">
                                    <h3>SHIPPING TO THE OTHER ADDRESS</h3>
                                </div>
                                <br>
                                <label>Other Notes</label>
                                <textarea rows="6" class="form-control" placeholder="Add extra notes..."></textarea>
                            </div>
                            <!-- end col -->
                        </div>
                        <!-- end row -->
                    </form>

                    <div class="payment_mth">
                        <div class="custom-title">
                            <h3>PAYMENT METHOD</h3>
                        </div>
                        <br>
                        <form id="payment" class="clearfix">
                            <label class="checkbox payment-method inline">
                                <input type="checkbox" id="customCheck1" value="option1" class=""> Direct Bank Transfer
                                <span class="custom2">Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order wont be shipped until the funds have cleared in our account.</span>
                            </label>
                            <label class="checkbox payment-method inline">
                                <input type="checkbox" id="customCheck2" value="option2"> Cheque Payment
                            </label>
                            <label class="checkbox payment-method inline">
                                <input type="checkbox" id="customCheck4" value="option2"> PayPal Payment
                            </label>
                            <label class="checkbox payment-method inline">
                                <input type="checkbox" id="customCheck3" value="option3"> Google Checkout
                            </label>
							<a href="#" class="more-link">PlACE AN ORDER</a>
                        </form>
                    </div>
                </div>
                <!-- end content -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </section>
    <!-- End: Checkout Section 
========================================-->



@include('includes.footer')


@endsection