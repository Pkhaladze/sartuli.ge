@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="index.html"> Home </a> / Contact </h4>
                    <h3>Contact Us</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->

    <!-- Start: contact  Section 
==================================================-->
    <section class="contact-section">
        <div class="container">
                <!-- Start: Heading -->
                <div class="base-header">
                    <h3>Get in <span class="ylow_clr">Touch</span></h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
                </div>
                <!-- End:  heading -->
            <!--  Content  -->
            <div class="inner-contact">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <!--  Contact Form  -->
                        <div class="contact-form">
                            <form method="post" action="mailer.php" id="contact-form">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input class="con-field" name="name" id="name" type="text" placeholder="Name">
                                    </div>
                                    <div class="col-lg-4">
                                        <input class="con-field" name="email" id="email" type="text" placeholder="Email">
                                    </div>
                                    <div class="col-lg-4">
                                        <input class="con-field" name="messageForm" id="messageForm" type="text" placeholder="Subject">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <textarea class="con-field" name="message" id="message" rows="6" placeholder="Your Message"></textarea>
                                        <div class="submit-area">
                                            <input type="submit" id="submit-contact" class="btn-alt" value="Send Message">
                                            <div id="msg" class="message"></div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- End:Contact Form  -->
                    </div>
                </div>
                <!-- row /- -->
            </div>
            <!-- End:Content  -->
        </div>
        <!-- container /- -->
    </section>
	
	<div id="map" class="map-container"></div>
	
    <!--End Contact Section
==================================================-->

@include('includes.footer')

@endsection