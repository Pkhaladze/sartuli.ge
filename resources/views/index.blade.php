@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')
@include('includes.slides_wrapper')
@include('includes.promotion')
@include('includes.welcome')
<!-- include('includes.services') -->
<!-- include('includes.avesome_facts') -->
@include('includes.offer')
<!-- include('includes.chooseus') -->
@include('includes.video_section')
@include('includes.gallery')
@include('includes.testimonial')
@include('includes.footer')

    <!-- START : Switcher 
========================================-->
    <div class="demo_changer">
      <div class="demo-icon"><i class="fa fa-gear fa-spin"></i></div>
      <div class="form_holder">
         <div class="layout_styles">
         <p>მხარდაჭერა</p>
<!--             <ul>
              <li><a href="index.html">Home</a></li>
              <li> <a href="single-shop.html">Single Shop Page</a></li>
              <li> <a href="shop.html">shop Page</a></li>
              <li> <a href="services.html">services Page</a></li>
              <li> <a href="gallery.html">gallery Page</a></li>
              <li> <a href="blog.html">blog Page</a></li>
              <li> <a href="contact.html">Contact Page</a></li>
              <li> <a href="about.html">about Page</a></li>
            </ul>  -->
          </div>
          <p>გადაღებე</p>
          <div class="predefined_styles"> 
              <a href="" rel="next" class="styleswitch"><img src="images/switcher/2.jpg" alt=""></a>    
              <a href="" rel="alternate" class="styleswitch"><img src="images/switcher/1.jpg" alt=""></a>
              <a href="" rel="prefetch" class="styleswitch"><img src="images/switcher/4.jpg" alt=""></a>
              <a href="" rel="bookmark" class="styleswitch"><img src="images/switcher/3.jpg" alt=""></a>
          </div>
          <span><a rel="tag" class="styleswitch" href="?default=true">RESET STYLE</a></span>
      </div>
    </div>
    <!-- END Switcher 
========================================-->

@endsection