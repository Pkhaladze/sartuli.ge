
    <!-- Start: About Section 
==================================================-->
    <section class="about-section aboutpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-5 col-xs-12">
                    <!--About right-->
                    <div class="about-right">
                        <div class="carousel slide" data-ride="carousel" id="blog-post-slider">
                            <a class="post-thumbnail" data-animsition-out="fade-out-up-sm" data-animsition-out-duration="500" href="single-blog.html">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item"><img alt="blog" src="images/background/about.jpg">
                                    </div>
                                    <div class="item active"><img alt="blog" src="images/background/about.jpg">
                                    </div>
                                </div>
                                <!-- Controls -->
                            </a>
                            <a class="left carousel-control" data-slide="prev" href="#blog-post-slider" role="button">
                                <span aria-hidden="true" class="fa fa-angle-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" data-slide="next" href="#blog-post-slider" role="button">
                                <span aria-hidden="true" class="fa fa-angle-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <!--/About right-->
                </div>
                <!--/.col-lg-6 col-md-8 col-md-7-->
                <div class="col-lg-6 col-sm-7 col-xs-12">
                    <!--About Left-->
                    <div class="about-left">
                        <h3>გაერთანებული ძალით <span class="ylow_clr"> გალე კაპიტალ კონსტრაქშენი </span> </h3>
                        <p>გალე წარმოადგენს ფართომაშტაბიან თანამშრომლობას ქართულ ბაზარზე მაღალი გამოცდილების მქონე სამშენებლო ჯგუფების, დეველოპერების, ინვესტორების და დისტრიბუტორების. ჩვენი მიზანია სამომხმარებლო ბაზრის სწრაფი და მოქნილი გადაყვანა ევროპული სამეზობლოს სტანდარტებზე. მომხმარებელთა მხარდაჭერა პარტნიორებთან ერთად ვაქციეთ მიღწევად მიზნად. უფრო და უფრო მეტი მშენებელი აძლევს ურთიერთ მაგალითს მაღალი სტანდარტების გადასვლისკენ. ჩვენი კიდევ ერთი ასეთი მიზანია ენერგოფექტური მშენებლობები და გარემოს დამაბინძურებელი სამშენებლო ნარჩენების მინიმალიზაცია.</p>
                    </div>
                    <!--/About Left-->
                </div>
                <!--/.col-lg-6 col-md-4 col-sm-5-->
            </div>
            <!--/.row-->
        </div>
        <!--/container-->
    </section>
    <!--  End:About Section
==================================================-->
