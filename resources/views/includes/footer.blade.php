
    <!-- Start:Footer Section 
==================================================-->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <!-- Start: About -->
                <div class="col-md-6 col-sm-4 col-xs-12">
                    <div class="widget">
                        <div class="col-md-12">
                            <img class="img-responsive" src="{{ asset('images/footer_logo.png') }}" alt="">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-2">
                            <ul class="list-icons link-list footer_soc">
                                <li>
                                    <a target="_blank" href="https://www.facebook.com/galecomge" class="fa fa-facebook"></a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.youtube.com/channel/UCrkLPx5DKhk6OZksdeiHCrg" class="fa fa-youtube"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <p class="footer_para">Loren ipsum dolor consectetur adipiscing elit sed do eiusmod tempor incididunt know you labore et dolore magna aliqua consectetur adipiscingLoren ipsum dolor consectetur adipiscing elit sed do eiusmod tempor incididunt </p>
                        </div>

                    </div>
                </div>
                <!-- End: About -->
                <!-- Start: Latest post -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                </div>
                <!-- End: Latest post -->
                <!-- Start:Tag -->
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="widget">
                        <h5>CONTACT INFO</h5>
                        <!-- Start Subscribe -->
                        <div class="box-content">
                            <div class="cont_widg">
                                <ul class="footer_contact_info">
                                    <li>
                                        <a href="mailto:galecomge@gmail.com">
                                            <i class="fa fa-envelope"></i>
                                            <span> galecomge@gmail.com</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="tel:+995597503434">
                                            <i class="fa fa-phone"></i>
                                            <span>+995 597 50 3434</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="tel:+995577193223">
                                            <i class="fa fa-phone"></i>
                                            <span> +995 577 193223 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <i class="icon_pin"></i>
                                        <span>თბილისი, ვაჟა-ფშაველას #41</span>
                                    </li>
                                    <li>
                                        <i class="icon_clock"></i>
                                        <span>ორშ. - პარ. 9am - 6 pm</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End:Tag -->
                <!-- Start:Subfooter -->
                <div class="subfooter">
                    <div class="col-xs-6">
                        <p>2017 © Copyright <a href="#">sartuli.ge</a> All rights Reserved.</p>
                    </div>
                    <div class="col-xs-6">
                        <a class="scrollup" href="#">GO TOP</a>
                    </div>
                </div> <!-- End:Subfooter -->
            </div>
        </div>
    </footer> <!-- End:Footer Section 
========================================-->