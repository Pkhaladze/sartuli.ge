    <!-- Start:Our Services Section
==================================================-->
  
<section class="service-section">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3>OUR <span class="ylow_clr"> SERVICES </span> </h3>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <div class="timeline_posts">
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <!-- about-item -->
                        <div class="service-item">
                            <div class="icon-serv  icon_desktop"></div>
                            <h5 class="serv-left">Floors &amp; Roofs</h5>
                            <div class="serv-info">Loren ipsum dolor sit consec sit tetur adipiscing elit sed do eiusmod tempo incididunt ut labore et doloreeiusmod tempor incididunt</div>
                        </div>
                        <!-- End: .about-item -->
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <!-- about-item -->
                        <div class="service-item">
                            <div class="icon-serv icon_lightbulb_alt"></div>
                            <h5 class="serv-left"> Rooms &amp; Halls</h5>
                            <div class="serv-info">Loren ipsum dolor sit consec sit tetur adipiscing elit sed do eiusmod tempo incididunt ut labore et doloreeiusmod tempor incididunt</div>

                        </div>
                        <!-- End: .about-item -->
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <!-- about-item -->
                        <div class="service-item">
                            <div class="icon-serv icon_clock_alt"></div>
                            <h5 class="serv-left">Basements</h5>
                            <div class="serv-info">Loren ipsum dolor sit consec sit tetur adipiscing elit sed do eiusmod tempo incididunt ut labore et doloreeiusmod tempor incididunt</div>
                        </div>
                        <!-- End: .about-item -->
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <!-- about-item -->
                        <div class="service-item">
                            <div class="icon-serv icon_camera_alt"></div>
                            <h5 class="serv-left">Consulting</h5>
                            <div class="serv-info">Loren ipsum dolor sit consec sit tetur adipiscing elit sed do eiusmod tempo incididunt ut labore et doloreeiusmod tempor incididunt</div>
                        </div>
                        <!-- End: .about-item -->
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <!-- about-item -->
                        <div class="service-item">
                            <div class="icon-serv icon_laptop"></div>
                            <h5 class="serv-left">Modern design</h5>
                            <div class="serv-info">Loren ipsum dolor sit consec sit tetur adipiscing elit sed do eiusmod tempo incididunt ut labore et doloreeiusmod tempor incididunt</div>
                        </div>
                        <!-- End: .about-item -->
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <!-- about-item -->
                        <div class="service-item">
                            <div class="icon-serv icon_genius"></div>
                            <h5 class="serv-left">24 Service </h5>
                            <div class="serv-info">Loren ipsum dolor sit consec sit tetur adipiscing elit sed do eiusmod tempo incididunt ut labore et doloreeiusmod tempor incididunt</div>
                        </div>
                        <!-- End: .about-item -->
                    </div>
                </div>
                <!-- /. row -->
            </div>
            <!-- /. row -->
        </div>
        <!-- /. container -->
    </section>

  
    <!-- End:Our Services Section
==================================================-->