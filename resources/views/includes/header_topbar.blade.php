    <!-- Start: Header Section 
==================================================-->
    <div class="header_topbar">
        <!-- Logo -->
        <div class="container">
            <div class="row">
                <div class="header_top_left">
                    <ul class="header_socil list-inline pull-left">
                        <li>
                            <a target="_blank" href="https://www.facebook.com/galecomge" class="fa fa-facebook"></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.youtube.com/channel/UCrkLPx5DKhk6OZksdeiHCrg" class="fa fa-youtube"></a>
                        </li>
                    </ul>
                </div>
                <div class="header_top_right list-unstyled">
                    <ul>
                        <li><a href="mailto:galecomge@gmail.com"><i class="fa fa-envelope"></i>galecomge@gmail.com</a></li>
                        <li><a href="tel:+995597503434"><i class="fa fa-phone"></i>+995 597 50 3434</a></li>
                        <li><a href="tel:+995577193223"><i class="fa fa-phone"></i>+995 577 193223</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End: Header Info -->