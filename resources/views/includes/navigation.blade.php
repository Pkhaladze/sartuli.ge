    <!-- Start: header navigation -->
    <div class="navigation">
        <div class="container">
            <div class="row">
                <div class="logo col-md-2">
                    <a href="/"><img class="img-responsive" src="{{ asset('images/logo.png') }}" alt="">
                    </a>
                </div>
                <div class="col-md-9 col-md-offset-1">
                    <div id="navigation">
                        <ul>
                            <li>
                                <a class="{{ $active == "home" ? "active" : "" }}" href="{{ asset('/') }}">მთავარი</a>
                            </li>
                            <li>
                                <a class="{{ $active == "products" ? "active" : "" }}" href="{{ url('/items') }}">
                                    პროდუქტები</a>
                            </li>
{{--                             <li>
                                <a class="{{ $active == "services" ? "active" : "" }}" href="{{ url('/services') }}">
                                    სერვისები</a>
                            </li> --}}
                            <li>
                                <a  href="{{ url('/gallery') }}" class="{{ $active == "gallery" ? "active" : "" }}">გალერეა</a>
                            </li>
                            <li>
                                <a  href="{{ url('/blog') }}" class="{{ $active == "blog" ? "active" : "" }}" >ბლოგები</a>
                            </li>
                            <li>
                                <a  href="{{ url('/contact') }}" class="{{ $active == "contact" ? "active" : "" }}">კონტაქტი</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End: social-nav -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
    </div>
    <!-- End: header navigation -->