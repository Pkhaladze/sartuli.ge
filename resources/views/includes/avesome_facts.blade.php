
    <!-- Start: Fun Facts Section 
==================================================-->
    <section class="confacts-section" data-stellar-background-ratio="0.7">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3>AWESOME   <span class="ylow_clr"> FACTS </span></h3>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <div class="col-sm-8 about_proj">
                    <!-- col-lg-4 -->
                    <div class="col-sm-4 col-xs-12">
                        <div class="icon-lay">
                            <i class="icon_gift_alt"></i>
                        </div>
                        <h3 class="stat-count count">197 </h3>
                        <h5>PROJECTS DONE</h5>
                    </div>
                    <!-- col-lg-4 /- -->
                    <!-- col-lg-4 -->
                    <div class="col-sm-4 col-xs-12">
                        <div class="icon-lay">
                            <i class="icon_lightbulb_alt"></i>
                        </div>
                        <h3 class="stat-count count">275</h3>
                        <h5>PROBLEMS SOLVED</h5>
                    </div>
                    <!-- col-lg-4 /- -->
                    <!-- col-lg-4 -->
                    <div class="col-sm-4 col-xs-12">
                        <div class="icon-lay">
                            <i class="icon_heart_alt"></i>
                        </div>
                        <h3 class="stat-count count">374</h3>
                        <h5> CLIENTS HELPED</h5>
                    </div>
                    <!-- col-lg-4 /- -->

                </div>
                <!-- row /- -->
            </div>
        </div>
    </section>
    <!-- Start:Fun Facts Section 
==================================================-->