
    <!-- Start: Testimonial Section
==================================================-->
    <div class="testimonials-section" id="testimo">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3> <span class="ylow_clr">თანამშრომლობა</span></h3>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
            </div>
            <!-- End: Heading -->
            <!-- End: Heading -->
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-xs-12">
                    <!-- Testimonials-->
                    <div class="owl-carousel owl-theme" id="testimonial">
                        <!-- Start: Testimonial 1 -->
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="col-lg-12 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, et verear noluisse eum, diam congue reformidans atomorum his id, pri te hinc expetenda. Est an mundi tollit iuvaret. An ius postulant reformidans. Vel an elit ludus fabellas, ex quando adipisci accommodare usuet verear noluisse eum diam</p>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-xs-12"><img alt="testimonial" src="images/background/tes1.jpg">
                                </div>
                                <div class="col-lg-8 col-sm-9 col-xs-12 cus-title">
                                    <span class="tes-nam">John Watson</span>
                                    <br>
                                    <span class="tes-degree">CEO At Google</span>
                                </div>
                            </div>
                            <!--End: Testimonial 1 -->
                        </div>
                        <!-- Start: Testimonial 3 -->
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="col-lg-12 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, et verear noluisse eum, diam congue reformidans atomorum his id, pri te hinc expetenda. Est an mundi tollit iuvaret. An ius postulant reformidans. Vel an elit ludus fabellas, ex quando adipisci accommodare usuet verear noluisse eum diam</p>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-xs-12"><img alt="testimonial" src="images/background/tes1.jpg">
                                </div>
                                <div class="col-lg-8 col-sm-9 col-xs-12 cus-title">
                                    <span class="tes-nam">John Watson</span>
                                    <br>
                                    <span class="tes-degree">CEO At Facebook</span>
                                </div>
                            </div>
                            <!--End: Testimonial 3 -->
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 client_warp">
                    <!-- Start: Client Section -->
                    <!-- client 1 -->
                    <div class="col-md-4">
                        <div class="client-box"><img alt="client" src="images/background/client_1.jpg">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="client-box"><img alt="client" src="images/background/client_2.jpg">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="client-box"><img alt="client" src="images/background/client_3.jpg">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="client-box"><img alt="client" src="images/background/client_4.jpg">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="client-box"><img alt="client" src="images/background/client_5.jpg">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="client-box"><img alt="client" src="images/background/client_3.jpg">
                        </div>
                    </div>
                </div>
                <!--/.col-lg-6-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- End: Testimonial Section 
==================================================-->
