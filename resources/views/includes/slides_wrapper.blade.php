	<!-- Start: Slides  -->
    <div class="slides_wrapper">
        <div class="slides__preload_wrapper">
            <div class="spinner"></div>
        </div>
        <div class="slider_home">
            <div class="single_slider slide_bg_1">
                <div class="slider_item_tb">
                    <div class="slider_item_tbcell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6">
									<h4>გალე კაპიტალ კონსტრაქშენი</h4>
                                    <h2>მშენებლობა   <span>  ყველგან</span></h2>
                                    <p> კერძო სახლების სექტორში ჩვენი მიზანი წარმოადგენს შეთავაზების ისეთ ფორმას, როდესაც დამკვეთი გვეტყვის, „სწორედ ეს მინდოდა“, ჩვენ ვიცით, რომ ურთიერთობები იწყება აღმოჩენით</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="single_slider slide_bg_2">
                <div class="slider_item_tb">
                    <div class="slider_item_tbcell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-6">
                                    <h4>გალე კაპიტალ კონსტრაქშენი</h4>
                                    <h2>მშენებლობა   <span>  ყველგან</span></h2>
                                    <p> კორპორატიული სახლები არის ურთიერთობის შეკრული და მოწესრიგებული ფორმა, გადანაწილებული საქმე, პასუხისმგებლობა და მხარდაჭერა</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End:  slider
==================================================-->