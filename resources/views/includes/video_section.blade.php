
    <!--Start: Video Section
==================================================-->
    <section class="video-section" id="video" data-stellar-background-ratio="0.4">
        <div class="overlay"></div>
        <div class="container">
            <div class="col-sm-12 col-xs-12">
                <div class="video-container">
                    <a data-backdrop="true" data-target="#video-modal" data-toggle="modal"><span class="play-video"><span class="fa fa-play"></span></span></a>
                </div>
                <!-- VIDEO SOCIALS -->
                <div class="video-content">
                    <h2>Watch our video tutorial</h2>
                </div>
                <!-- END VIDEO SOCIALS -->
            </div>
        </div>
    </section>
    <!-- VIDEO POPUP -->
    <div class="modal fade video-modal" id="video-modal" role="dialog">
        <div class="modal-content">
            <div class="row">
                <iframe allowfullscreen="" height="400" src="https://www.youtube.com/embed/BzMLA8YIgG0" width="712"></iframe>
            </div>
        </div>
    </div>
    <!-- End: VIDEO Section
==================================================-->
