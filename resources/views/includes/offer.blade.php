    <!-- Start: We offer Section
==================================================-->
    <section class="whoffer-section">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3>რას   <span class="ylow_clr"> გთავაზობთ </span></h3>
                <p>შევასრულოთ მხარდამჭერი როლი თქვენი სახლის, საცხოვრებელი კომპლექსის თუ საქმის კომფორტულად გადაწყვეტაში</p>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <div class="owl-carousel owl-theme" id="whoffer">
                    <!-- Start: Testimonial 1 -->
                    <div class="item">
                        <!-- About 1-->
                        <div class="whoffer-list">
                            <div class="whoffer-img">
                                <img alt="team" class="img-responsive" src="images/background/offer1.jpg">
                            </div>
                            <div class="whoffer-mask">
                                <a href="services.html"><i class="icon_link"></i></a>
                            </div>
                            <div class="whoffer-info">
                                <h6>მოქმედების სტრატეგია</h6>
                                <p>თითოეული მონაწილე მნიშვნელოვნად ავითარებს სრულფასოვან გადაწყვეტას, გონივრულ დროსა და სივრცეში</p>
                            </div>
                        </div>
                    </div>
                    <!-- end About 1-->
                    <!-- About 2-->
                    <div class="item">
                        <div class="whoffer-list">
                            <div class="whoffer-img"><img alt="team" class="img-responsive" src="images/background/offer2.jpg">
                            </div>
                            <div class="whoffer-mask">
                                <a href="services.html"><i class="icon_link"></i></a>
                            </div>
                            <div class="whoffer-info">
                                <h6>სასიამოვნო ტრენდი</h6>
                                <p>მიანდოთ თქვენი გეგმები მათ, ვინც ავითარებს შესაძლებლობებს გარკვეული იყოთ ყველა დეტალში, ისევე როგორც საკუთარ თავში</p>
                            </div>
                        </div>
                    </div>
                    <!-- end About 2-->
                    <!-- About 3-->
                    <div class="item">
                        <div class="whoffer-list">
                            <div class="whoffer-img"><img alt="team" class="img-responsive" src="images/background/offer3.jpg">
                            </div>
                            <div class="whoffer-mask">
                                <a href="services.html"><i class="icon_link"></i></a>
                            </div>
                            <div class="whoffer-info">
                                <h6>დაცულობა მნიშვნელოვანია</h6>
                                <p>თითოეული ჩართულობა მშენებლობისა თუ დაგეგმარების ეტაპზე მნიშვნელოვანია იყოს ზედმიწევნით და შეთანხმებულად</p>
                            </div>
                        </div>
                    </div>
                    <!-- end About 3 -->

                </div>
                <!---/.row -->
            </div>
            <!--/.container -->
        </div>
        <!---/.row -->
        </div>
        <!--/.container -->
    </section>
    <!-- End: We offer Section
==================================================-->
