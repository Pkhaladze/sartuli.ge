 
<!-- Start:Our Work Section 
==================================================-->  
<section class="work-section" id="work">
    <div class="container">
					<!-- Start: Heading -->
        <div class="base-header">
            <h3>our <span class="ylow_clr">Gallery</span> </h3>
            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
        </div><!-- End:  heading -->	
 
                <!-- Project Filter-->
                <div class="project-filter">
                    <ul class="project_menu">
                        <li class="filter hvr-underline-from-center" data-filter="all"><i class="fa fa-database"></i> All Works</li>
                        <li class="filter hvr-underline-from-center" data-filter=".category-1"><i class="fa fa-file-image-o"></i>Garden</li>
                        <li class="filter hvr-underline-from-center" data-filter=".category-2"><i class="fa fa-file-code-o"></i> Building</li>
                        <li class="filter hvr-underline-from-center" data-filter=".category-3"><i class="fa fa-file-text-o"></i> Construction</li>
                        <li class="filter hvr-underline-from-center" data-filter=".category-4"><i class="fa fa-file-video-o"></i> Interior</li>
                    </ul>
                </div>
				<!-- Project Filter End-->
 
        <div class="row">
			<!-- col-lg-12 col-sm-6 -->
            <div class="col-lg-12 col-sm-12">
                <div class="project-list">					
					<!-- Work Item 1 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-2" data-myorder="1">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-1.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-1.jpg">
									<i class="icon_plus"></i>
								</a>
						
							</div>
						</div>
					</div>
					<!--/ End: Work Item 1 -->
					<!-- Work Item 2 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-3" data-myorder="2">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-2.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 2 -->
					<!-- Work Item 3 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-4" data-myorder="3">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-3.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-3.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 3 -->
					<!-- Work Item 4 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-1" data-myorder="4">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-4.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-4.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 4 -->
					<!-- Work Item 5 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-2" data-myorder="1">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-5.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-5.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 5 -->
					<!-- Work Item 6 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-3" data-myorder="2">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-6.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-6.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ Work Item 6 -->
					<!--/ End: Work Item 7 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-2" data-myorder="1">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-7.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-7.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 7 -->
					<!-- Work Item 8 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-3" data-myorder="2">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-2.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-8.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 8 -->
					<!--/ End: Work Item 9 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-2" data-myorder="1">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-1.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-1.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 9 -->
					<!-- Work Item 10 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-3" data-myorder="2">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-8.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-2.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 10 -->
					<!--/ End: Work Item 11 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-2" data-myorder="1">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-1.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-3.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 11 -->
					<!-- Work Item 12 -->
					<div class="col-lg-3 col-sm-3 col-xs-12 mix category-3" data-myorder="2">
						<div class="work-item">
							<div class="items"><img alt="" class="" src="images/shop/work-6.jpg"></div>
							<div class="mask2">
								<a href="#"><h4>BUILDINGS</h4></a>
								<a href="#"><p>Interior</p></a>
								<a class="info2" data-lightbox="example-2" data-title="Optional caption." href="images/shop/work-4.jpg">
									<i class="icon_plus"></i>
								</a>
							</div>
						</div>
					</div>
					<!--/ End: Work Item 12 -->
				</div><!--/project-list --> 
            </div>
			<!--/.col-lg-12 col-sm-6-->
        </div>
		<!--/.row-->
    </div>
    <!--/.container-->
</section>
<!-- End:Our Work Section 
==================================================-->