    <!-- Start: Choose Us Section
==================================================-->
    <section class="chooseus-section">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3>Why <span class="ylow_clr"> Choose </span></h3>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
            </div>
            <!-- End: Heading -->
            <div class="row">

                <div class="chooseus-warper col-lg-6 col-sm-7 col-xs-12">
                    <div class="col-sm-12 servic_accrodin">
                        <div id="accordion">
                            <section id="item4" class="ac_hidden">
                                <p class="pointer">&#9654;</p>
                                <h2><a href="#">100% CUSTOMERS SUPPORT</a></h2>
                                <h4>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur </h4>
                            </section>
                            <section id="item1">
                                <p class="pointer">&#9654;</p>
                                <h2><a href="#">high quality standarts</a></h2>
                                <h4>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur </h4>
                            </section>
                            <section id="item2" class="ac_hidden">
                                <p class="pointer">&#9654;</p>
                                <h2><a href="#">We have reasonable prices</a></h2>
                                <h4>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur </h4>
                            </section>
                            <section id="item3" class="ac_hidden">
                                <p class="pointer">&#9654;</p>
                                <h2><a href="#">we are RELIABLE COMPANY</a></h2>
                                <h4>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur </h4>
                            </section>
							<section id="item5" class="ac_hidden">
                                <p class="pointer">&#9654;</p>
                                <h2><a href="#">high quality standarts</a></h2>
                                <h4>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur </h4>
                            </section>
                        </div>
                    </div>
                </div>
                <!-- /.chooseus-warper-->
                <div class="col-lg-6 col-sm-5 col-xs-12" id="scene">
                    <div class="chooseus-left layer" data-depth="0.50">
						<img alt="" src="images/background/choose.jpg">
                    </div>
                    <!-- End: chooseus-left -->
                </div>

            </div>
            <!-- /. row -->
        </div>
        <!-- /. container -->
    </section>
    <!-- End: Choose Us Section
==================================================-->