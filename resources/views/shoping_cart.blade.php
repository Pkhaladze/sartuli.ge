@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="index.html"> Home </a> / <a href="index.html"> Shop </a> / cart </h4>
                    <h3>Shoping cart</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->


    <!-- Start: Cart Section
==================================================-->
    <div class="shop_cart">
        <div class="container">
            <div class="big-title">
                <div class="pull-left">
                    <h2>Shopping Cart</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="table-responsive text-center">
                        <table class="table table-bordered">
                            <thead>
                                <tr class="shop_cart_tr">
                                    <th class="text-center">Product</th>
                                    <th class="text-center">Products name</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-center">total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="table-info">
                                    <td class="prod">
                                        <a href="#"><img src="images/shop/th2.jpg" alt="">
                                        </a>
                                    </td>
                                    <td class="ptitle"><a href="#">Samsung Mobile</a>
                                    </td>
                                    <td class="unit"><span>$160.00</span>
                                    </td>
                                    <td class="qty">
                                        <input value="1" type="text">
                                    </td>
                                    <td class="unit"><span>$160.00</span>
                                    </td>
                                    <td><a href="#"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr class="table-info">
                                    <td class="prod">
                                        <a href="#"><img src="images/shop/th2.jpg" alt="">
                                        </a>
                                    </td>
                                    <td class="ptitle"><a href="#"> Samsung Laptop</a>
                                    </td>
                                    <td class="unit"><span>$178.00</span>
                                    </td>
                                    <td class="qty">
                                        <input value="1" type="text">
                                    </td>
                                    <td class="unit"><span>$457.00</span>
                                    </td>
                                    <td><a href="#"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shop_cart_bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="discount-coupon">
                        <h4 class="text-uppercase">Discount Codes</h4>
                        <p>Enter your coupon code if you have one.</p>
                        <form action="#">
                            <input class="coupon" type="text">
                        </form>
                        <a class="app-coupon" href="#">APPLY COUPON</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="estimate-ship">
                        <h4 class="text-uppercase">Estimate Shipping and Tax</h4>
                        <p>Enter your destination to get a shipping estimate.</p>
                        <div class="country">
                            <p class="title-country">Country</p>
                            <select name="sort-by-country" class="sort-by-country">
                                <option selected="selected" value="bang">Bangladesh</option>
                                <option value="eng">England</option>
                                <option value="ger">Germany</option>
                            </select>
                        </div>
                        <div class="state">
                            <p class="title-state">State/Province</p>
                            <select name="sort-by-state" class="sort-by-state">
                                <option selected="selected" value="dhk">Dhaka</option>
                                <option value="bar">Sylhet</option>
                                <option value="khl">Comilla</option>
                            </select>
                        </div>
                        <div class="postal-code">
                            <p class="p-code">Zip/Postal Code</p>
                            <form action="#">
                                <input class="post-code" type="text">
                            </form>
                        </div>
                        <a class="get-quote" href="#">GET A QUOTE</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="grand-total-area">
                        <h4 class="text-uppercase">Cart Totals</h4>
                        <p class="sub-total">Subtotal: $211.00</p>
                        <h2 class="grand-total">order total: <span class="amt">$160.00</span></h2>
                        <a class="pro-checkout text-uppercase" href="#">PROCEED TO CHECK OUT</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End: Cart Section 
========================================-->



@include('includes.footer')
@endsection