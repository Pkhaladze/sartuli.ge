@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->


@include('includes.header_topbar')

@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="index.html"> Home </a> / About </h4>
                    <h3>About Us</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->


@include('includes.welcome')

    <!--  Start:Team Section
==================================================-->
    <section class="team-section">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3>Team <span class="ylow_clr">member</span></h3>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <div class="owl-carousel owl-theme" id="team">
                    <!-- Start: Testimonial 1 -->
                    <div class="item">
                        <!-- profile 1-->
                        <div class="member">
                            <div class="img">
                                <div class="timg_overlay">
                                    <img alt="team" class="img-responsive" src="images/team/2.jpg">
                                </div>
                                <div class="team_icon"><a href="team.html"><i class="icon_link"></i></a>
                                </div>
                                <div class="info">
                                    <h6>Bob Martines</h6>
                                    <p>Co Founder</p>
                                    <ul class="social list-inline">
                                        <li>
                                            <a href="#" title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Follow Us on Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Watch Our Videos"><i class="fa fa-youtube-play"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End: profile 1-->
                    <!-- profile 2-->
                    <div class="item">
                        <div class="member">
                            <div class="img">
                                <div class="timg_overlay">
                                    <img alt="team" class="img-responsive" src="images/team/1.jpg">
                                </div>
                                <div class="team_icon"><a href="team.html"><i class="icon_link"></i></a>
                                </div>
                                <div class="info">
                                    <h6>Jak Martines</h6>
                                    <p>Designer</p>
                                    <ul class="social list-inline">
                                        <li>
                                            <a href="#" title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Follow Us on Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Watch Our Videos"><i class="fa fa-youtube-play"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End: profile 2-->
                    <!-- profile 3-->
                    <div class="item">
                        <div class="member">
                            <div class="img">
                                <div class="timg_overlay">
                                    <img alt="team" class="img-responsive" src="images/team/3.jpg">
                                </div>
                                <div class="team_icon"><a href="team.html"><i class="icon_link"></i></a>
                                </div>
                                <div class="info">
                                    <h6>Jon Martines</h6>
                                    <p>Designer</p>
                                    <ul class="social list-inline">
                                        <li>
                                            <a href="#" title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Follow Us on Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Watch Our Videos"><i class="fa fa-youtube-play"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End: profile 3 -->
                    <!-- profile 4-->
                    <div class="item">

                        <div class="member">
                            <div class="img">
                                <div class="timg_overlay">
                                    <img alt="team" class="img-responsive" src="images/team/4.jpg">
                                </div>
                                <div class="team_icon"><a href="team.html"><i class="icon_link"></i></a>
                                </div>
                                <div class="info">
                                    <h6>Adam Martines</h6>
                                    <p>Director</p>
                                    <ul class="social list-inline">
                                        <li>
                                            <a href="#" title="Follow Us on Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Follow Us on Twitter"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="#" title="Watch Our Videos"><i class="fa fa-youtube-play"></i></a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- End: profile 4 -->
                </div>
            </div>
            <!---/.row -->
        </div>
        <!--/.container -->
    </section>
    <!--  End:Team Section 
==================================================-->

<!-- Start: Testimonial Section
==================================================-->
    <div class="testimonials-section testimonial_pg" id="testimo">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3>our <span class="ylow_clr">Testimonial</span></h3>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing elit. Phasellus id lectus quis dui euismod con placerat massa nec elit egestas efficitur. </p>
            </div>
            <!-- End: Heading -->
            <!-- End: Heading -->
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-xs-12">
                    <!-- Testimonials-->
                    <div class="owl-carousel owl-theme" id="testimonial">
                        <!-- Start: Testimonial 1 -->
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="col-lg-12 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, et verear noluisse eum, diam congue reformidans atomorum his id, pri te hinc expetenda. Est an mundi tollit iuvaret. An ius postulant reformidans. Vel an elit ludus fabellas, ex quando adipisci accommodare usuet verear noluisse eum diam</p>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-xs-12"><img alt="testimonial" src="images/background/tes1.jpg">
                                </div>
                                <div class="col-lg-8 col-sm-9 col-xs-12 cus-title">
                                    <span class="tes-nam">John Watson</span>
                                    <br>
                                    <span class="tes-degree">CEO At Google</span>
                                </div>
                            </div>
                            <!--End: Testimonial 1 -->
                        </div>
                        <!-- Start: Testimonial 3 -->
                        <div class="item">
                            <div class="testimonial-box">
                                <div class="col-lg-12 col-sm-12">
                                    <p>Lorem ipsum dolor sit amet, et verear noluisse eum, diam congue reformidans atomorum his id, pri te hinc expetenda. Est an mundi tollit iuvaret. An ius postulant reformidans. Vel an elit ludus fabellas, ex quando adipisci accommodare usuet verear noluisse eum diam</p>
                                </div>
                                <div class="col-lg-3 col-sm-3 col-xs-12"><img alt="testimonial" src="images/background/tes1.jpg">
                                </div>
                                <div class="col-lg-8 col-sm-9 col-xs-12 cus-title">
                                    <span class="tes-nam">John Watson</span>
                                    <br>
                                    <span class="tes-degree">CEO At Facebook</span>
                                </div>
                            </div>
                            <!--End: Testimonial 3 -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!-- End: Testimonial Section 
==================================================-->

@include('includes.footer')



@endsection