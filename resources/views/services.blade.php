@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="{{ url('/') }}"> Home </a> / წინადადებები </h4>
                    <h3>წინადადებები</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->

    <!-- Start:Our Services Section
==================================================-->


<!-- დასამატებელია სერვისები -->


    <!-- End:Our Services Section
==================================================-->


<!-- Start: Fun Facts Section 
==================================================-->
    <section class="confacts-section">
        <div class="container">
            <!-- Start: Heading -->
            <div class="base-header">
                <h3>AWESOME   <span class="ylow_clr"> FACTS </span></h3>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <div class="col-sm-8 about_proj">
                    <!-- col-lg-4 -->
                    <div class="col-sm-4 col-xs-12">
                        <div class="icon-lay">
                            <i class="icon_gift_alt"></i>
                        </div>
                        <h3 class="stat-count count">197 </h3>
                        <h5>PROJECTS DONE</h5>
                    </div>
                    <!-- col-lg-4 /- -->
                    <!-- col-lg-4 -->
                    <div class="col-sm-4 col-xs-12">
                        <div class="icon-lay">
                            <i class="icon_lightbulb_alt"></i>
                        </div>
                        <h3 class="stat-count count">275</h3>
                        <h5>PROBLEMS SOLVED</h5>
                    </div>
                    <!-- col-lg-4 /- -->
                    <!-- col-lg-4 -->
                    <div class="col-sm-4 col-xs-12">
                        <div class="icon-lay">
                            <i class="icon_heart_alt"></i>
                        </div>
                        <h3 class="stat-count count">374</h3>
                        <h5> CLIENTS HELPED</h5>
                    </div>
                    <!-- col-lg-4 /- -->

                </div>
                <!-- row /- -->
            </div>
        </div>
    </section>
    <!-- Start:Fun Facts Section 
==================================================-->

    <!-- Start: Pricing Section 
==================================================-->
    <section class="pricing-section" id="pricing">
        <!-- container -->
        <div class="container">
			<!-- Start: Heading -->
            <div class="base-header">
                <h3>ჩვენი <span class="ylow_clr"> ფასები </span> </h3>
            </div>
            <!-- End: Heading -->
            <div class="row">
                <div class="col-sm-3 col-xs-12"> <!-- Start: pricing-box 1 -->
                    <div class="pricing-box">
                        <h2><span><i class="lari lari-normal"></i></span>360<sub>1 მ<sup>2</sup>-დან</sub></h2>
                        <h3>შავი კარკასი (სახლი)</h3>
                        <p>პროექტირება/ნებართვა</p>
                        <p>მონოლითი და კედლები</p>
                        <p>გადახურვა</p>
                        <p>კომუნიკაციები</p>
                        <a href="{{ url('/contact') }}" class="more-link">დაგვიკავშირდით</a>
                    </div>    <!-- End: pricing-box 1 -->
                </div>
                <div class="col-sm-3 col-xs-12">  <!-- Start: pricing-box 1 -->
                    <div class="pricing-box">
                        <h2><span><i class="lari lari-normal"></i></span>455<sub>1 მ<sup>2</sup>-დან</sub></h2>
                        <h3>თეთრი კარკასი (სახლი)</h3>
                        <p>შიდა-გარე ლესვა</p>
                        <p>კარ-ფანჯრები</p>
                        <p>შიდა ქსელები წერტილებად</p>
                        <p>იატაკის მოჭიმვ</p>
                        <a href="{{ url('/contact') }}" class="more-link">დაგვიკავშირდით</a>
                    </div>   <!-- End: pricing-box 1 -->
                </div>
                <div class="col-sm-3 col-xs-12"> <!-- Start: pricing-box 1 -->
                    <div class="pricing-box">
                        <h2><span><i class="lari lari-normal"></i></span>760<sub>1 მ<sup>2</sup>-დან</sub></h2>
                        <h3>შავი (კორპორატიული)</h3>
                        <p>პროექტირება/ნებართვა</p>
                        <p>ლიფტი და ფასადი</p>
                        <p>კარ-ფანჯრები</p>
                        <p>კომუნიკაციები</p>
                        <a href="{{ url('/contact') }}" class="more-link">დაგვიკავშირდით</a>
                    </div><!-- End: pricing-box 1 -->
                </div>
                <div class="col-sm-3 col-xs-12"> <!-- Start: pricing-box 1 -->
                    <div class="pricing-box">
                        <h2><span><i class="lari lari-normal"></i></span>860<sub>1 მ<sup>2</sup>-დან</sub></h2>
                        <h3>თეთრი (კორპორატიული)</h3>
                        <p>ლესვა</p>
                        <p>ლიფტი და ფასადი</p>
                        <p>შიდა ქსელები</p>
                        <p>იატაკის მოჭიმვა</p>
                        <a href="{{ url('/contact') }}" class="more-link">დაგვიკავშირდით</a>
                    </div> <!-- End: pricing-box 1 -->
                </div>
            </div><!--/ row -->
        </div><!--/ container -->
    </section><!--End: Pricing Section 
==================================================-->


@include('includes.footer')

@endsection