@extends('layouts.site_master')
@section('content')
    <!-- ::::::::::::::::::::::::::: Start: Preloader section ::::::::::::::::::::::::::: -->
    <div id="preloader"></div>
    <!-- ::::::::::::::::::::::::::: End: Preloader section ::::::::::::::::::::::::::: -->
@include('includes.header_topbar')
@include('includes.navigation')

    <!-- header -->
    <header id="page-top" class="blog-banner">
        <!-- Start: Header Content -->
        <div class="container" id="blog">
            <div class="row blog-header text-center wow fadeInUp" data-wow-delay="0.5s">
                <div class="col-sm-12">
                    <!-- Headline Goes Here -->
                    <h4><a href="index.html"> Home </a> <a href="blog.html">  / Blog </a>  / Single Blog </h4>
                    <h3>Single Blog</h3>
                </div>
            </div>
            <!-- End: .row -->
        </div>
        <!-- End: Header Content -->
    </header>
    <!--/. header -->

    <!-- End: Header Section
==================================================-->


    <!-- Start : Blog Page Content  
==================================================-->
    <div class="blog_container no-padding single_blog_container">
        <div class="container">
            <div class="row">
                <!-- Blog Area -->
                <div class="col-md-9 col-sm-8 col-xs-12 blog-area">
                    <div class="blog-post-list wow fadeInLeft" data-wow-delay="0.3s">
								<div class="entry-cover">
									<a href="single-blog.html"><img alt="blog-1" src="images/blog/post2.jpg">
									</a>
								</div>
								<h3 class="entry-title"><a href="single-blog.html" title="The big leagues our turn Straightnin">This is full width blog post</a></h3>
								<div class="entry-meta">
									<span class="entry-author"><a href="#" title="Andreanne Turcotte">By : Adam Turcotte</a></span>
									<span class="entry-date"><a href="#" title="October 13, 2015"> 28 / Feb , 2017</a></span>
									<span class="entry-comments"> <a href="#" title="Tag">Comments : 4</a></span>
								</div>

								<div class="entry-content">
									<p>Lorem ipsum dolor sit amet, in urna molestie tristique.Cong erment sed at facilisis lacinia aliquam fusce wisi, porta ligula nibh vel congue diam. Sed ligula erat molestie cras morbi in facilisis eu elit Lorem ipsum dolor sit amet, in urna molestie tristique.Cong erment sed at facilisis lacinia aliquam fusce wisi, porta ligula nibh vel congue diam. Sed ligula erat molestie cras morbi in in urna molestie tristique.Cong erment sed at facilisis lacinia aliquam fusce wisi</p>
								</div>
								<a href="#" class="more-link"> Read More</a>
							</div>
                    <!--/ article -->
                    <div class="post-option clearfix">
                        <div class="pull-left">
                            <a href="#" class="prev-post"><span class="arrow-icon arrow_carrot-left"></span> &nbsp;PREV POST</a>
                        </div>
                        <span class="middle-icon"><a href="blog.html" class="icon_grid-3x3"></a></span>
                        <div class="pull-right">
                            <a href="#" class="next-post">NEXT POST &nbsp;<span class="arrow-icon arrow_carrot-right"></span></a>
                        </div>
                    </div>
                    <!--comments list -->
                    <div class="list-comments">
                        <div class="comments-section-title">
                            <h4>Comments On This Post</h4>
                        </div>
                        <!-- .section-title -->
                        <ul class="comments">
                            <li>
                                <div class="comment">
                                    <img src="images/blog/comment.jpg" alt="" class="comment-avatar">
                                    <strong class="commenter-title"><a href="#">John Doe</a></strong>
                                    <span class="comment-date">27 Jan 2015</span>
                                    <p>Lorem ipsum dolor sit amet, in urna molestie tristique. A fermentum sed at, facilisis lacinia aliquam fusce wisi, porta ligula nibh vel congue diam. Sed ligula erat molestie cras morbi in, facilisis eu elit, ac suscipit pellentesque, praesent fringilla sit elit amet in suspendisse</p>
                                    <span class="comment-reply"><a href="#">Reply</a></span>
                                </div>
                                <!-- .comment -->
                                <ul>
                                    <li>
                                        <div class="comment">
                                            <img src="images/blog/comment.jpg" alt="" class="comment-avatar">
                                            <strong class="commenter-title"><a href="#">John Doe</a></strong>
                                            <span class="comment-date">27 June 2016</span>
                                            <p>Lorem ipsum dolor sit amet, in urna molestie tristique. A fermentum sed at, facilisis lacinia aliquam fusce wisi, porta ligula nibh vel congue diam. Sed ligula erat molestie cras morbi in, facilisis eu elit, ac suscipit pellentesque, praesent fringilla sit elit amet in suspendisse</p>
                                            <span class="comment-reply"><a href="#">Reply</a></span>
                                        </div>
                                        <!-- .comment -->
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--/ .comments -->
                    </div>
                    <!--/comments list -->

                    <!-- comment box   -->
                    <div class="contact">
                        <div class="contact-form-warper blog-contact">
                            <div class="col-lg-12 col-sm-12 col-xs-12 contact-warper">
                                <!--  Contact Form  -->
                                <div class="contact-form">
                                    <form action="mailer.php" id="contactForm" method="post" name="contactForm">
                                        <div class="form-group col-sm-4">
                                            <input class="form-control" id="Name" name="Name" placeholder="Name :" type="text">
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <input class="form-control" id="Email" name="Email" placeholder="Email :" type="text">
                                        </div>
                                        <div class="form-group col-sm-4">
                                            <input class="form-control" id="Tel" name="Tel" placeholder="Number :" type="text">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" id="Message" name="Message" placeholder="Message :"></textarea>
                                        </div>
                                        <input class="submit-button btn btn-chos" name="submit" value="Submit" type="submit">
                                    </form>
                                </div>
                                <!-- End:Contact Form  -->
                            </div>
                        </div>
                    </div>
                    <!--/.comment box-->
                </div>
                <!--/ Blog Area -->

                <!-- Widget Area -->
                <div class="col-md-3 col-sm-4 col-xs-12 widget-area">
                    <!-- Widget Search -->
                    <aside class="widget widget-search">
                        <!-- input-group -->
                        <div class="input-group">
                            <input class="form-control" placeholder="Search" type="text">
                            <span class="input-group-btn">
								<button type="button"><i class="fa fa-search"></i></button>
							</span>
                        </div>
                        <!-- /input-group -->
                    </aside>
                    <!-- Widget Search /- -->
                    <!-- About Author -->

                    <aside class="widget widget-post-categories">
                        <h3 class="widget-title">Post Categories</h3>
                        <ul class="categories-type">
                            <li>
                                <a href="#" title="Business">Computer</a>
                            </li>
                            <li>
                                <a href="#" title="Wordpress">Laptop</a>
                            </li>
                            <li>
                                <a href="#" title="Theme Forest">Monitor</a>
                            </li>
                            <li>
                                <a href="#" title="Web Developement">Mobile</a>
                            </li>
                            <li>
                                <a href="#" title="Statistics">Phone</a>
                            </li>
                        </ul>
                    </aside>
                    <!-- Post Categories /- -->
                    <!-- Recent Post -->
                    <aside class="widget wiget-recent-post">
                        <h3 class="widget-title">Recent Post</h3>
                        <div class="recent-post-box">
                            <div class="recent-title">
                                <a href="blog-post.html">Laptop Repair</a>
                                <p>25th Jan 2015</p>
                            </div>
                        </div>
                        <div class="recent-post-box">
                            <div class="recent-title">
                                <a href="blog-post.html">Phone Repair</a>
                                <p>25th Feb 2015</p>
                            </div>
                        </div>
                        <div class="recent-post-box">
                            <div class="recent-title">
                                <a href="blog-post.html">Desktop Repair</a>
                                <p>25th Jun 2015</p>
                            </div>
                        </div>
                    </aside>
                    <!-- Recent Post /- -->
                    <!-- Widget Instagram -->
                    <aside class="widget widget-instagram">
                        <h3 class="widget-title">Instagram</h3>
                        <div class="instagram-item">
                            <ul class="instagram-photo-list">
                                <li>
                                    <a href="#"><img alt="" class="img-responsive" src="images/blog/ins1.jpg">
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" class="img-responsive" src="images/blog/ins2.jpg">
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" class="img-responsive" src="images/blog/ins3.jpg">
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><img alt="" class="img-responsive" src="images/blog/ins4.jpg">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    <!-- Widget Instagram /- -->
                    <!-- Widget Tags -->
                    <aside class="widget widget-tags">
                        <h3 class="widget-title">Top Tags</h3><a href="#" title="Install">Install</a> <a href="#" title="Design">Design</a> <a href="#" title="Video">Video</a> <a href="#" title="Branding">Branding</a> <a href="#" title="Pakaging">Pakaging</a>
                    </aside>
                    <!-- Widget Tags /- -->
                </div>
                <!-- Widget Area /- -->
            </div>
        </div>
        <!-- Container /- -->
    </div>
    <!-- End : Blog Page Content 
==================================================-->

@include('includes.footer')
@endsection