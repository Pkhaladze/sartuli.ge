@extends('layouts.admin_master')
@section('content')

<div class="container">

	
<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th>#</th>
      <th>სახელი</th>
      <th>ზომის ერთეული</th>
      <th>ფასი</th>
      <th>ფასდაკლება</th>
      <th>განახლება</th>
    </tr>
  </thead>
  <tbody>
	@foreach ($items as $item)
	    <tr>
	      <th scope="row">{{ $item->id }}</th>
	      <td >{{ $item->name }}</td>
	      <td >{{ $item->units }}</td>
	      <td class="text-center">{{ $item->price }}</td>
	      <td class="text-center">{{ $item->sale }}</td>
	      <td class="text-center">
	      	<a href="{{ url('/admin/'. $item->id .'/edit') }}">
	      		<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
	      	</a>
	      </td>
	    </tr>
	  @endforeach
  </tbody>
</table>





</div>


@endsection