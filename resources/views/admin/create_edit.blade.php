@extends('layouts.admin_master')
@section('content')
<div class="container">
   @if (session()->has('success'))
        <div class="col-md-offset-2 col-md-8" >
              <div class="alert alert-success">
                <strong>{{ session('success') }}</strong>
              </div>
        </div>
    @endif
    @if (isset($errors) && count($errors) > 0)
        <ul class="list-group">
            @foreach ($errors->all() as $error)
                <li class="list-group-item list-group-item-danger">
                    <strong>{{ $error }}</strong>
                </li>
            @endforeach
        </ul>
    @endif

	<form method="POST" action="{{ url('admin') }}{{isset($id) ? '/'.$id : "" }} " enctype="multipart/form-data">
		{!! csrf_field() !!}
		<div class="form-row">
			<div class="form-group col-md-4">
				<label for="name" class="col-form-label">სახელი <span>*</span> </label>
				<input type="text" class="form-control" value="{{ $item->name }}" id="name" name="name" placeholder="სახელი">
			</div>
			<div class="form-group col-md-4">
				<label for="units" class="col-form-label">ზომის ერთეული <span>*</span> </label>
				<input type="text" class="form-control" value="{{ $item->units }}" id="units" name="units" placeholder="ზომის ერთეული">
			</div>
			<div class="form-group col-md-4">
				<label for="price" class="col-form-label">ფასი <span>*</span> </label>
				<input type="text" class="form-control" value="{{ $item->price }}" id="price" name="price" placeholder="ფასი">
			</div>
			<div class="form-group col-md-4">
				<label for="sale" class="col-form-label">ფასდაკლებით </label>
				<input type="text" value="{{ $item->sale }}" class="form-control" id="sale" name="sale" placeholder="ფასდაკლებით">
			</div>
			<div class="form-group col-md-4">
				<label for="quantity" class="col-form-label">რაოდენობა <span>*</span> </label>
				<input type="text" class="form-control" value="{{ $item->quantity }}" id="quantity" name="quantity" placeholder="რაოდენობა">
			</div>
			<div class="form-group col-md-4">
				<label for="status" class="col-form-label">სტატუსი <span>*</span> </label>
				<select  id="status" name="status" class="form-control">
					<option>active</option>
					<option>passive</option>
				</select>
			</div>

			<div class="form-group col-md-4">
				<label for="categories" class="col-form-label">კატეგორიები <span>*</span> </label>  
			    <select id="categories" class="form-control input-md" name="categories[]" multiple="multiple">
			    	<?php foreach ($categories as $category) {
			    		$selected = "";
			    		foreach ($item->categories as $value) {
			    			if ($category->id == $value->id) {
			    				$selected = "selected='selected'";
			    			}
			    		}
			    	?>
						<option value="{{$category->id }}" {{ $selected }}>
							{{ $category->name }}
						</option>
			    	<?php } ?>
			   </select>
			</div>

		    <div class="clearfix"></div>
			<div class="form-group col-md-6">
				<label for="short_description" class="col-form-label">მოკლე აღწერა</label>
				<textarea rows="3" class="form-control"  id="short_description" name="short_description" placeholder="მოკლე აღწერა">{{ $item->short_description }}</textarea> 
			</div>
			<div class="form-group col-md-6">
				<label for="description" class="col-form-label">აღწერა</label>
				<textarea rows="3" class="form-control" id="description" name="description" placeholder="აღწერა">{{ $item->description }}</textarea> 
			</div>
			<div class="clearfix"></div>
		</div>
	    <div id="photo">
	    @if (isset($id))
	    	@php $i = 1; @endphp
	    	@foreach ($photos as $photo)
		    	<div class="form-group col-xs-11">
		    		<label for="file_input_{{ $i }}" class="edit_photo left" style="background-image: url('{{ url('/img/items/'.$id.'/small/'. $photo->file_name) }}')">
		    		<span >შეცვლა</span>
		            </label>
		            <input class="left" id="file_input_{{ $i }}" type="file" name="photos[{{ $photo->file_name }}]">
		        </div>
		        @php $i +=1; @endphp
	    	@endforeach
	    @else
	        <div class="form-group col-xs-11">
	            <label for="file_input" class="col-lg-2">Photo</label>
	            <input id="file_input" type="file" name="photos[]">
	        </div>
	    @endif

	    </div>
	        <p type="button" class="btn btn-primary col-xs-1" id="upl_photo"> + </p>
	    <div class="clearfix"></div>
		<hr>
		@if (isset($id))
            <button type="submit" id="add" name="add" class="btn btn-primary">განახლება</button>
            <input type="hidden" value="put" name="_method">                    
        @else 
            <button type="submit" id="add" name="add" class="btn btn-primary">დამატება</button>
        @endif
	</form>

	<script type="text/javascript">
	        $('#upl_photo').click(function(){
	            $p =  '<div class="form-group col-xs-11"><label for="file_input" class="col-lg-2">Photo</label><input id="file_input" type="file" name="photos[]"></div>';
	            $("#photo").append($p);
	        });

	</script>
	
</div>


@endsection