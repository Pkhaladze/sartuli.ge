<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

	
	public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'category_item');
    }

	public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

}
