<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function items()
    {
        return $this->belongsToMany('App\Models\Item', 'category_item');
    }
}
