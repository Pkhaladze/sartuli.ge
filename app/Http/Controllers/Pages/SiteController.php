<?php

namespace App\Http\Controllers\Pages;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Item;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = "home";
        return view('index', compact("active")); 
    }

    public function about()
    {
       return view('about'); 
    }
    public function shop()
    {
        return view(); 
    }

    public function blog()
    {
        $active = "blog";
        return view('blog', compact('active'));
    }
    public function single_blog()
    {
        //return "skdngsdkjgkjfd";
       return view('single_blog'); 
    }

    public function gallery()
    {
        $active = "gallery";
        return view('gallery', compact('active'));
    }

    public function single_gallery()
    {
        
        return view('single_gallery'); 
    }

    public function services()
    {
        $active = "services";
        return view('services', compact('active')); 
    }

    public function contact()
    {
        $active = "contact";
        return view('contact', compact('active')); 
    }

   
}
