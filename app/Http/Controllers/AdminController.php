<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Item;
use App\Models\Category;
use DB;
use Storage;
use Validator;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('admin.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = Category::all();
        $item = new Item;
        return view('admin.create_edit', compact('categories', 'item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules = [
            'name' => 'required|max:100',
            'units' => 'required|max:50',
            'price' => 'required|max:10',
            'quantity' => 'required|max:10',
            'status' => 'required',
            'categories' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'photos' => 'required',
        ];

        try {
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($request->all())
                    ->withErrors($validator);
            }
            DB::beginTransaction();
            $item = new Item;
            $item->name = $request->name;
            $item->units = $request->units;
            $item->price = $request->price;
            $item->sale = $request->sale;
            $item->quantity = $request->quantity;
            $item->status = $request->status;
            $item->short_description = $request->short_description;
            $item->description = $request->description;
            $item->save();

            //make relationship with categories
            foreach ($request->categories as $category) {
                $item->categories()->attach($category);
            }

            //add photo
            $i = 1;
            foreach ($request->file('photos') as $file) {
                $ext = $file->guessClientExtension();
                $imageName = time() . str_random(3) . '.' . $ext;

                $photo = new Photo;
                $photo->file_name = $imageName;
                $photo->item_id = $item->id;
                $photo->priority = 0;
                if ($i == 1) {
                    $photo->priority = $i;
                }
                $photo->save();

                //save laege image
                Storage::makeDirectory('/img/items/' . $item->id . '/large/');
                $laege_path = '../storage/app/img/items/' . $item->id . '/large/';
                $image = Image::make($file->getRealPath())->resize(360, 400);
                $image->save($laege_path . $imageName);

                //save small image
                Storage::makeDirectory('/img/items/' . $item->id . '/small/');
                $small_path = '../storage/app/img/items/' . $item->id . '/small/';
                $image = Image::make($file->getRealPath())->resize(101, 111);
                $image->save($small_path . $imageName);
                $i +=1;
            }
            DB::commit();
        } catch (Exception $e) {
                DB::rollBack();
            }
        return redirect()->back()->with('success', "წარმატებით დაემატა მონაცემები!");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        $categories = Category::all();
        $photos = Photo::where('item_id', $id)->get();
        return view('admin.create_edit', compact('categories', 'item', 'id', 'photos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
                    'name' => 'required|max:100',
                    'units' => 'required|max:50',
                    'price' => 'required|max:10',
                    'quantity' => 'required|max:10',
                    'status' => 'required',
                    'categories' => 'required',
                    'short_description' => 'required',
                    'description' => 'required',
                ];

        try {
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withInput($request->all())
                    ->withErrors($validator);
            }
            DB::beginTransaction();
            $item =Item::find($id);
            $item->name = $request->name;
            $item->units = $request->units;
            $item->price = $request->price;
            $item->sale = $request->sale;
            $item->quantity = $request->quantity;
            $item->status = $request->status;
            $item->short_description = $request->short_description;
            $item->description = $request->description;
            $item->save();
            
             //update relationship with categories
            $item->categories()->detach();
            foreach ($request->categories as $category) {
                $item->categories()->attach($category);
            }

            //add ar adit photo
            if ($request->file('photos') !== null) {
                foreach ($request->file('photos') as $key => $file) {
                    $ext = $file->guessClientExtension();
                    $imageName = time() . str_random(3) . '.' . $ext;

                    $photo = Photo::where('item_id', $id)->where('file_name', $key)->get();
                    if ($photo->count()) {
                        $imageName = $photo[0]->file_name;
                    }else{
                        $photo = new Photo;
                        $photo->file_name = $imageName;
                        $photo->item_id = $item->id;
                        $photo->priority = 0;
                        $photo->save();
                    }


                    //save laege image
                    $laege_path = '../storage/app/img/items/' . $item->id . '/large/';
                    $image = Image::make($file->getRealPath())->resize(360, 400);
                    $image->save($laege_path . $imageName);

                    //save small image
                    $small_path = '../storage/app/img/items/' . $item->id . '/small/';
                    $image = Image::make($file->getRealPath())->resize(101, 111);
                    $image->save($small_path . $imageName);
                }
            }
            DB::commit();
        } catch (Exception $e) {
                DB::rollBack();
            }
        return redirect()->back()->with('success', "წარმატებით შეიცვალა მონაცემები.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
